
//pri<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   
});

//primera de controladorBG:
Route::get("categorias" , "categoriaController@index");

//ruta que nos muestre el formulario para crear categoria
Route::get("categorias/create", "CategoriaController@create");
//ruta para guardar la nueva categoria en BD 
Route::post("categorias/store", "CategoriaController@create");
