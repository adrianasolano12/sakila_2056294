<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SAKILA- Lista de categorias</title>
</head>
<body>
    <h1> Lista de categorias</h1>
    <table>
       <thead>
           <tr>
               <th>
                  Nombre de la categoria
               </th>
         </tr>
    </thead>
    <tbody>
    @foreach($categorias as $c)
       <tr>
           <td>
           {{ $c->name }}
           </td>
           </tr>
    @endforeach
    </tbody>
    </table>
</body>
</html>